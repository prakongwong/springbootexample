package com.springboot.example.backend.exception;

public class BaseException extends Exception{

    public BaseException(String message){
        super(message);
    }
}
