package com.springboot.example.backend.exception;

public class FileException extends BaseException{

    public FileException(String code) {
        super("file." + code);
    }

    public static FileException fileNull(){
        return new FileException("null");
    }

    public static FileException fileMaxsize(){
        return new FileException("max.size");
    }

    public static FileException fileUnsupport(){
        return new FileException("unsupported.file.type");
    }
}



