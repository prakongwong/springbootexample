package com.springboot.example.backend.model;

import lombok.Data;

@Data
public class TestResponse {

    private String name;
    private String food;
}
