package com.springboot.example.backend.model;

import lombok.Data;

@Data
public class MRegisterResponse {

    private String userId;
    private String email;
    private String name;

}
