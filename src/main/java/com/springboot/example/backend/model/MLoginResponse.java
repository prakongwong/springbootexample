package com.springboot.example.backend.model;

import lombok.Data;

@Data
public class MLoginResponse {

    private String accessToken;
}
