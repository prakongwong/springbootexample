package com.springboot.example.backend.model;

import lombok.Data;

@Data
public class MLoginRequest {

    private String email;
    private String password;
}
