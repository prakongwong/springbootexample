package com.springboot.example.backend.mapper;

import com.springboot.example.backend.entity.User;
import com.springboot.example.backend.model.MRegisterResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring") //ระบุว่าใช้กับ Component ของ Spring
public interface UserMapper {

    MRegisterResponse toRegisterResponse(User user);
}
