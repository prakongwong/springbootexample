package com.springboot.example.backend.business;

import com.springboot.example.backend.entity.User;
import com.springboot.example.backend.exception.BaseException;
import com.springboot.example.backend.exception.FileException;
import com.springboot.example.backend.exception.UserException;
import com.springboot.example.backend.mapper.UserMapper;
import com.springboot.example.backend.model.MLoginRequest;
import com.springboot.example.backend.model.MLoginResponse;
import com.springboot.example.backend.model.MRegisterRequest;
import com.springboot.example.backend.model.MRegisterResponse;
import com.springboot.example.backend.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UserBusiness {

    private final UserService userService;
    private final UserMapper userMapper;

    public UserBusiness(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    public MLoginResponse login(MLoginRequest requset) throws BaseException {
        // validate requset

        // verify database
        Optional<User> opt = userService.findByEmail(requset.getEmail());
        if (opt.isEmpty()) {
            throw UserException.loginFailEmailNotFound();
        }

        User user = opt.get();
        if (!userService.matchPassword(requset.getPassword(), user.getPassword())) {
            throw UserException.loginFailPasswordIncorrect();
        }

        // TODO: Generate JWT
        String accessToken = "JWT TO DO";
        MLoginResponse mLoginResponse = new MLoginResponse();
        mLoginResponse.setAccessToken(accessToken);

        return mLoginResponse;
    }

    public MRegisterResponse register(MRegisterRequest request) throws BaseException {
        User user = userService.create(request.getEmail(), request.getPassword(), request.getName());
        MRegisterResponse mRegisterResponse = userMapper.toRegisterResponse(user);
        mRegisterResponse.setUserId(user.getId());

        return mRegisterResponse;
    }

    public String uploadPicture(MultipartFile file) throws BaseException {
        //validate file
        if (file == null) {
            throw FileException.fileNull();
        }

        //validate size
        if (file.getSize() > 1048576 * 2) {
            throw FileException.fileMaxsize();
        }

        //validate contentType
        String contentType = file.getContentType();
        if (contentType == null) {
            throw FileException.fileUnsupport();
        }
        List<String> supportedType = Arrays.asList("image/jpeg", "image/png");
        if (supportedType.contains(contentType)) {
            throw FileException.fileUnsupport();
        }

        // TODO: upload file File Storage(AWS S3, etc..)
        try {
            byte[] bytes = file.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

}
