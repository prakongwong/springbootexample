package com.springboot.example.backend.business;

import com.springboot.example.backend.exception.ProductException;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ProductBusiness {

    private com.springboot.example.backend.exception.ProductException ProductException;

    public String getProductById(String id) throws ProductException {
        // TODO: get data from Database
        if(Objects.equals("1234", id)){
            throw ProductException.notFound();
        }

        return id;
    }

}
